// FILE: tlmx_packet.c
#include "tlmx_packet.h"
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <assert.h>
#include <sstream>



const char* tlmx_command_to_str(const tlmx_command_t & s) {
  switch(s) {
    case TLMX_IGNORE      : return "TLMX_IGNORE";
    case TLMX_READ        : return "TLMX_READ";
    case TLMX_WRITE       : return "TLMX_WRITE";
    case TLMX_DEBUG_READ  : return "TLMX_DEBUG_READ";
    case TLMX_DEBUG_WRITE : return "TLMX_DEBUG_WRITE";
    case TLMX_EXIT        : return "TLMX_EXIT";
    default               : return "*UNKNOWN_STATUS_ERROR*";
  }
}

const char* tlmx_status_to_str(const tlmx_status_t & s) {
  switch(s) {
    case TLMX_INCOMPLETE_RESPONSE    : return "TLMX_INCOMPLETE_RESPONSE";
    case TLMX_OK_RESPONSE            : return "TLMX_OK_RESPONSE";
    case TLMX_ADDRESS_ERROR_RESPONSE : return "TLMX_ADDRESS_ERROR_RESPONSE";
    case TLMX_GENERIC_ERROR_RESPONSE : return "TLMX_GENERIC_ERROR_RESPONSE";
    default                          : return "*UNKNOWN_STATUS_ERROR*";
  }
}

std::shared_ptr<tlmx_packet> new_tlmx_packet( const uint8_t & command, const uint64_t & address , const uint16_t & data_len , void* data_ptr )
{ return std::make_shared<tlmx_packet>( command, address, data_len, data_ptr ); }
std::shared_ptr<tlmx_packet> clone_tlmx_packet( const tlmx_packet & rhs)
{ return new_tlmx_packet( rhs.command, rhs.address, rhs.data_len, rhs.data_ptr ); }
void delete_tlmx_packet( std::shared_ptr<tlmx_packet> payload_ptr){ payload_ptr.reset(); }
int  pack_tlmx( char buffer[], tlmx_packet & tp ){ return tp.pack( buffer ); }
int  unpack_tlmx( tlmx_packet & tp, const char buffer[]){ return tp.unpack( buffer ); }

int get_tlmx_data_len(char buffer[])
{
  int data_len;
  assert(buffer != NULL);
  memcpy( &data_len, &buffer[ TLMX_DATA_LEN_INDEX ], TLMX_PACKET_DATA_LEN_SIZE  );
  return data_len;
}

size_t tlmx_packet::pack(char *buffer ) const
{
    memcpy( & buffer[ TLMX_ID_INDEX       ], & id, TLMX_PACKET_ID_SIZE       );
    memcpy( & buffer[ TLMX_COMMAND_INDEX  ], & command, TLMX_PACKET_COMMAND_SIZE  );
    memcpy( & buffer[ TLMX_STATUS_INDEX   ], & status, TLMX_PACKET_STATUS_SIZE   );
    memcpy( & buffer[ TLMX_ADDRESS_INDEX  ], & address, TLMX_PACKET_ADDRESS_SIZE  );
    memcpy( & buffer[ TLMX_DATA_LEN_INDEX ], & data_len , TLMX_PACKET_DATA_LEN_SIZE );
    memcpy( & buffer[ TLMX_DATA_PTR_INDEX ], data_ptr, data_len  );
    if constexpr ( debug > 1 ) print( __PRETTY_FUNCTION__ );
    return TLMX_DATA_PTR_INDEX + data_len;
}


size_t tlmx_packet::unpack( const char *buffer )
{
    memcpy( &(id       ), &buffer[ TLMX_ID_INDEX       ], TLMX_PACKET_ID_SIZE        );
    memcpy( &(command  ), &buffer[ TLMX_COMMAND_INDEX  ], TLMX_PACKET_COMMAND_SIZE   );
    memcpy( &(status   ), &buffer[ TLMX_STATUS_INDEX   ], TLMX_PACKET_STATUS_SIZE    );
    memcpy( &(address  ), &buffer[ TLMX_ADDRESS_INDEX  ], TLMX_PACKET_ADDRESS_SIZE   );
    memcpy( &(data_len ), &buffer[ TLMX_DATA_LEN_INDEX ], TLMX_PACKET_DATA_LEN_SIZE  );
    memcpy( data_ptr, & buffer[TLMX_DATA_PTR_INDEX], data_len  );
    if constexpr ( debug > 4 ) print( __PRETTY_FUNCTION__ );
    return TLMX_DATA_PTR_INDEX + data_len;
}

std::string tlmx_packet::print(const char *backtrace) const
{
    std::stringstream oss;
    oss << backtrace << " "
        << tlmx_command_to_str( static_cast<tlmx_command_t>(command) )
        << " "
        << tlmx_status_to_str( static_cast<tlmx_status_t>(status) )
        << " r" << address << std::hex << " $" << data_ptr << "(" << data_len << ")" ;
    if( command == TLMX_WRITE || ( ( command == TLMX_READ ) && ( status == TLMX_OK_RESPONSE ) ) )
        oss << "(" << serialize( data_ptr, data_len ) << ")";
    oss << std::endl;
    return oss.str();
}

std::string tlmx_packet::serialize( const void *buffer, const int so_buffer ) const
{
    char tmp[so_buffer];
    strcpy(tmp,"");
    for( int i = 0; i < so_buffer; i++ )
        sprintf( tmp + strlen( tmp ), "%02x",  ((uint8_t*)(buffer))[ i ] );
    return tmp;
}

std::string tlmx_packet::serialize( ) const {  return serialize( data_ptr, data_len ); }
