#include "driver.h"
#include "tlmx_packet.h"
#include <netdb.h>
#include <sys/socket.h>
#include <sys/errno.h>
#include <cassert>
#include <cstring>
static char               hostip[100]; /*< hold string rep of IP # */
static int                tcpip_port;
static int                server_stop     = 0; //< indicates server should stop

static constexpr const char* acknowledgement = "Ack\n";
static int constexpr length(const char* str){ return *str ? 1 + length(str + 1) : 0;}

sysCDevice::sysCDevice(const std::string &hostname, const int & port) : Module<sysCDevice>("")
{
    tcpip_port = DefaultPort;
    if ( port != 0 ) tcpip_port = port;

    try
    {
        if ( struct hostent* hostentry = gethostbyname( hostname.c_str() ); hostentry == nullptr )
            throw ("Failed to gethostbyname");
        else
        {
            struct in_addr** addr_list = (struct in_addr **) hostentry->h_addr_list;
            for ( int i = 0; addr_list[i] != nullptr; ++i )
                strcpy( hostip, inet_ntoa(*addr_list[i]) );
        }
        sC_server.sin_addr.s_addr = inet_addr(hostip);
        sC_server.sin_family = AF_INET;

        for( auto & sock : std::vector<int*>{ & sC_sock, & safety_sock } )
            if ( ( *sock = socket(AF_INET, SOCK_STREAM, 0 ) ) < 0 )
                throw ": socket creation";

        if( sockConnect( sC_sock, port ) )
            if( connectionComplete( port  ) )
                state = OnLine();

    }
    catch ( const char* message )
    {
        std::stringstream err;
        err << __PRETTY_FUNCTION__ << message << "errno(" << strerror(errno) << ")";
        error( err.str() );
        return;
    }

}

bool sysCDevice::sockConnect(const int sock, const int port)
{
    auto connection_failure = [&]( const char* message ="" )
    {
        std::stringstream ss;
        ss << message << " errno(" << errno << ") " << strerror(errno);
        error( message );
        return false;
    };

    {
        std::stringstream ss;
        ss << __PRETTY_FUNCTION__ << " : Searching SystemC connector addressed as " << hostip << ":" << port;
        auditor(ss);
    }

    const Millisec waitConnection{1000};
    int tries = 0, connect_status = 0;
    sC_server.sin_port = htons( port );
    do
    {
        ++tries;
        if (( connect_status = connect( sock, (struct sockaddr *)&sC_server, sizeof(sC_server)) ) < 0)
        {
            std::stringstream ss;
            ss << "Waiting for SystemC socket connection( " << errno << " " << strerror(errno) << " ," << connect_status << ")";
            auditor(ss);
            std::this_thread::sleep_for( waitConnection );
            continue;
        }
        break;

    } while( tries < 5 );

    return ( connect_status < 0 ) ? connection_failure( __PRETTY_FUNCTION__ ) : true;
}

bool sysCDevice::connectionComplete( const int port )
{
    std::future<int> fut = main_promise.get_future();
    std::future<bool> interrupt = interrupt_promise.get_future();
    std::thread( & sysCDevice::interrupt_server, this, std::ref(fut) ).detach();
    main_promise.set_value( port );

    std::stringstream ss;
    ss << __PRETTY_FUNCTION__ << ": waiting promise on systemC connection";
    auditor( ss );
    return interrupt.get();
}

void sysCDevice::interrupt_server(std::future<int> &future )
{
    std::string backtrace( __PRETTY_FUNCTION__ );

    auto report =[&]( const std::string & message, bool error = true )
    {
        std::stringstream ss;
        ss << message;
        if( error )
            ss << " errno(" << errno << ") : " << strerror(errno);
        auditor( ss, error ? Error : Verbose );
        return;
    };

    report( backtrace + ": waiting master-promise trigger", false );
    int port = future.get() +1;

    int listening_socket;
    if( listening_socket = socket(AF_INET, SOCK_STREAM, 0); listening_socket < 0 )
        return report( backtrace + ": listener socket creation failure " );

    if ( int option_value = 1; setsockopt( listening_socket, SOL_SOCKET, SO_REUSEADDR, (void* )&option_value, (socklen_t)(sizeof(socklen_t))) < 0)
        return report( backtrace + ": unable to set socket option");

    struct sockaddr_in local_server;
    local_server.sin_family = AF_INET;
    local_server.sin_addr.s_addr = INADDR_ANY;
    local_server.sin_port = htons( port );
    if ( bind( listening_socket, (struct sockaddr *)&local_server , sizeof(local_server) ) < 0 )
        return report( backtrace + ": bind failure");

    listen(listening_socket, 2);
    interrupt_promise.set_value( true );

    for(;;)
    {
        report( backtrace + ": waiting for incoming connections on port:" + std::to_string(port), false );
        constexpr int addr_len = sizeof(struct sockaddr_in);

        struct sockaddr_in tmp;
        int sock = accept( listening_socket, (struct sockaddr *)&tmp, (socklen_t*)&addr_len );
        if ( sock < 0 ) {
            report( backtrace +": loop Accept failed" );
            continue;
        }

        report( backtrace + ": Connection accepted", false );
        if ( server_stop )
        {
            close(sock);
            return;
        }

        constexpr int ack_size = length( acknowledgement )+1;
        if ( write( sock, acknowledgement, ack_size) < 0)
        {
            report( backtrace + ": Acknowledgement write/send failed" );
            continue;
        }
        close(sock);
    }

}

#include <array>
sysCDevice::~sysCDevice()
{
    std::stringstream ss;
    ss << __PRETTY_FUNCTION__;

    auditor( ss );
    server_stop = 1;
    if( std::holds_alternative<sysCDevice::OnLine>( state ) )
        safetyPort("Shutdown\n");
    std::array<int,2> sockets = { sC_sock, safety_sock };
    ranges::for_each( sockets, []( int socket ){ close(socket); } );
    return;
}

#include <vector>
void sysCDevice::safetyPort( const char* irq_message )
{
    if( sockConnect( safety_sock, tcpip_port + 1 ) )
    {
        std::vector<uint8_t> reply( strlen(irq_message) );
        write( safety_sock, irq_message, reply.size() );
        if ( int recv_count = read( safety_sock, reply.data(), reply.size() );  recv_count < 0 )
        {
            std::stringstream ss;
            ss << __PRETTY_FUNCTION__ << ": missed ACK on termination " << irq_message << " command";
            error( ss.str(), Warning );
        }
    }
}

int sysCDevice::transport(const tlmx_command_t &command, const addr_t &address, void *data_ptr, const dlen_t &data_len)
{
    std::stringstream oss;
    auto packet = new_tlmx_packet( command, address, data_len, data_ptr );

    std::array<char,TLMX_MAX_BUFFER> message;
    size_t so_xmitt = packet->pack( message.data() );
    if ( write( sC_sock, message.data(), so_xmitt) < 0 )
        error("TCPIP write/send failed to send all data");
    std::array<char,TLMX_MAX_BUFFER> reply;
    if( size_t so_reply = so_xmitt; read( sC_sock, reply.data(), so_reply ) < 0 )
        error("TCPIP read/recv didn't receive enough data");

    auto payload_recv_ptr = clone_tlmx_packet( *packet.get() );
    payload_recv_ptr->unpack( reply.data() );
    return ( payload_recv_ptr->status == TLMX_OK_RESPONSE) ? 0 : -1;
}

void sysCDevice::error( const std::string & error, Numerology level )
{
    std::stringstream ss;
    ss << error;
    auditor( ss, level );
}

