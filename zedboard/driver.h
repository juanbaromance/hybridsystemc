#pragma once

#include <stdint.h>

typedef uint64_t      addr_t;
using data_t = uint8_t;
typedef uint16_t      dlen_t;

#include "util.h"
#include "tlmx_packet.h"

template <typename T>
class iPLDevice : public crtp<T,iPLDevice> {
public:

    template< bool debug = false>
    int put( const addr_t & address , const void* payload, const dlen_t &so_payload)
    { return this->underly().transport( debug ? TLMX_DEBUG_WRITE : TLMX_WRITE, address, const_cast<void*>(payload), so_payload ); }

    template<bool debug = false>
    int get( const addr_t & address , void* data_ptr , const dlen_t & data_len )
    { return this->underly().transport( debug ? TLMX_DEBUG_READ : TLMX_READ, address, data_ptr, data_len ); }

    struct OnLine{};
    struct OffLine{};
    std::variant<OffLine,OnLine> state{OffLine()};

};

template<>
class iPLDevice<void> {
public:
    enum {
        base     = 0, /*0x8000FF080100*/
        sr       = ( base + 0*4 ),
        counter1 = ( base + 1*4 ),
        counter2 = ( base + 2*4 ),
        counter3 = ( base + 3*4 ),
        counter4 = ( base + 4*4 ),
    };
};


#include <arpa/inet.h>
#include <variant>
#include <future>
class sysCDevice : public iPLDevice<sysCDevice>, public Module<sysCDevice> {

public :
    sysCDevice( const std::string & hostname, const int & hostport );
    ~sysCDevice();
    static constexpr int DefaultPort{4000};

private:
    struct sockaddr_in sC_server;
    friend iPLDevice<sysCDevice>;

    int  transport( const tlmx_command_t & command, const addr_t & address, void *data_ptr, const dlen_t & data_len  );
    void error(const std::string & error , Numerology level= Error );
    bool connectionComplete( const int port );
    int sC_sock, safety_sock;
    void safetyPort( const char *irq_message = "Software interrupt\n" );
    bool sockConnect( const int sock, const int port );

    std::promise<int> main_promise;
    std::promise<bool> interrupt_promise;
    void interrupt_server(std::future<int> &  );
};




