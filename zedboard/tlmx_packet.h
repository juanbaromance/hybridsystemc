﻿#pragma once

#include <stdint.h>
#include <string>

typedef enum
{
    TLMX_INCOMPLETE_RESPONSE,
    TLMX_OK_RESPONSE,
    TLMX_ADDRESS_ERROR_RESPONSE,
    TLMX_GENERIC_ERROR_RESPONSE /*< must be last */
} tlmx_status_t;
const char* tlmx_status_to_str(const tlmx_status_t & s);

#include <sstream>
#include <string>
#include <iostream>
struct tlmx_packet
{
  tlmx_packet( const uint8_t & command_, const uint64_t & address_, const uint16_t & data_len_, void *data_ptr_  )
        : command( command_ ), address( address_ ), data_len( data_len_ ), data_ptr( data_ptr_ ),
          id( s_id++ ), status( TLMX_INCOMPLETE_RESPONSE )
  {}

  ~tlmx_packet(void){}

  bool operator==(const tlmx_packet& rhs) const;
  tlmx_packet& operator=(const tlmx_packet& rhs);

  size_t pack( char* buffer ) const;
  bool compare( const tlmx_packet & ) const;

  size_t  unpack(const char *buffer);
  std::string str(void) const;
  std::string print( const char *backtrace) const;


  int32_t  id;      
  uint8_t  command; 
  uint8_t  status;  
  uint64_t address; 
  uint16_t data_len;
  void*    data_ptr;


public:
  std::string serialize() const;

private:
  inline static int32_t s_id = -1;
  static constexpr int debug = 2;
  std::string serialize( const void *buffer, const int so_buffer ) const;
};

#include <memory>
typedef std::shared_ptr<tlmx_packet> tlmx_packet_ptr;
enum TLMNumerology
{
    TLMX_PACKET_COMMAND_SIZE = 1,
    TLMX_PACKET_ID_SIZE     =  4,
    TLMX_PACKET_STATUS_SIZE  = 1,
    TLMX_PACKET_ADDRESS_SIZE = 8,
    TLMX_PACKET_DATA_LEN_SIZE = 2,
    TLMX_ID_INDEX    =  0,
    TLMX_COMMAND_INDEX  = (TLMX_ID_INDEX       + TLMX_PACKET_ID_SIZE)  ,
    TLMX_STATUS_INDEX   =(TLMX_COMMAND_INDEX  + TLMX_PACKET_COMMAND_SIZE) ,
    TLMX_ADDRESS_INDEX  =(TLMX_STATUS_INDEX   + TLMX_PACKET_STATUS_SIZE) ,
    TLMX_DATA_LEN_INDEX =(TLMX_ADDRESS_INDEX  + TLMX_PACKET_ADDRESS_SIZE),
    TLMX_DATA_PTR_INDEX =(TLMX_DATA_LEN_INDEX + TLMX_PACKET_DATA_LEN_SIZE),
    TLMX_MAX_DATA_LEN   =((1<<(8*TLMX_PACKET_DATA_LEN_SIZE))-1),
    TLMX_MAX_BUFFER     =(TLMX_DATA_PTR_INDEX + TLMX_MAX_DATA_LEN),
};


typedef enum
{
    TLMX_READ,
    TLMX_WRITE,
    TLMX_IGNORE,
    TLMX_DEBUG_READ,
    TLMX_DEBUG_WRITE,
    TLMX_EXIT /*< must be last */
} tlmx_command_t;

const char* tlmx_command_to_str(const tlmx_command_t & s);

#include <memory>
std::shared_ptr<tlmx_packet>  new_tlmx_packet( const uint8_t & command, const uint64_t & address , const uint16_t & data_len, void* data_ptr );
std::shared_ptr<tlmx_packet>  clone_tlmx_packet( const tlmx_packet & rhs );
int  pack_tlmx(char buffer[], tlmx_packet &payload);
int  unpack_tlmx( tlmx_packet& packet, const char buffer[]);
void print_tlmx ( const tlmx_packet& payload_ptr, tlmx_packet &message);
int  get_tlmx_data_len( char buffer[] );
