#include "driver.h"
#include "util.h"

#include <array>
#include <cassert>

class MyApp : private Module<MyApp>
{

private:
    static constexpr int TESTCNT = 1000000; /* number of tests to perform */
    static constexpr int Fatal = true;

public:
    MyApp( int argc, char *argv[] ) : Module<MyApp>( __PRETTY_FUNCTION__ )
    {
        try {
            std::stringstream oss;
            if ( argc == 3 )
            {
                char* endptr = nullptr;
                if( long int port = strtol( argv[2], & endptr, 10 ); port >= 2000 && port <= 65535 )
                {
                    pl_dev = std::make_shared<sysCDevice>( std::string( argv[1] ), port );
                    if( std::holds_alternative<iPLDevice<sysCDevice>::OnLine>( pl_dev->state ) )
                    {
                        oss << "Device connection : ready -> Run available";
                        auditor( oss );
                        return;
                    }
                    oss << "SystemC server is still grounded";
                }
                else
                    oss << "port number " << port << ": OutOfBound : parameter is restricted between 2000 and 65535";
                throw oss;
            }
            oss << " Program Syntax : " << argv[ 0 ] << " Hostname PortNumber";
            throw oss;
        }
        catch ( std::stringstream & oss )
        {
            auditor( oss, Error );
            assert(0);
        }
    }

    void run()
    {
        srandom(1);
        auto test = [&]( int index )
        {
            pushPattern();
            if ( int payload; pl_dev->get( iPLDevice<void>::sr, & payload, sizeof( payload ) ) < 0 )
            {
                std::stringstream err;
                err << __PRETTY_FUNCTION__ << " : iteration<" << index << "> status-ack Failure : l o s t";
                communications_failure( err, Fatal );
            }
            pullPattern();
        };

        for( int i = 0; i != TESTCNT; ++i )
            test( i );

        std::stringstream oss;
        oss << __PRETTY_FUNCTION__ << ": done";
        auditor(oss);
    }

private:
    size_t error{0},index{0};
    static constexpr int REGCNT  =  4; /* number of registers in device*/
    std::array<int,REGCNT> buffer{0};
    int payload;

    void pushPattern()
    {
        index = 0;
        auto sync_pattern = [&](auto)
        {
            payload = abs( random() % 5000 ) + 1000;
            auto offset = iPLDevice<void>::counter1 + 4 * index;

            if ( pl_dev->put( offset, & payload, sizeof( payload ) ) < 0 )
            {
                std::stringstream err;
                err << "xmitt Failure on offset(" << std::dec << offset << ")payload( " << payload << " : $" << std::hex << payload << " )";
                communications_failure( err );
                return;
            }
            buffer[ index ] = payload;
            index = ( ++index ) % buffer.size();
        };
        ranges::for_each( buffer, sync_pattern );

    }

    void pullPattern()
    {
        index = 0;
        auto readout = [&]( auto backup )
        {
            auto offset = iPLDevice<void>::counter1 + 4*index;
            if ( pl_dev->get( offset, & payload, sizeof(payload) ) < 0 )
            {
                std::stringstream err;
                err << "receive Failure on offset(" << offset << ") setting(" << payload << ")";
                communications_failure( err );
                return;
            }
            if( backup ^ payload )
            {
                std::stringstream err;
                err << "Consistency Failure on offset(" << offset << ") " << payload << " versus " << backup;
                communications_failure( err );
            }
            index = ( ++index ) % buffer.size();
        };
        ranges::for_each( buffer, readout );
    }

    void communications_failure( std::stringstream & oss, bool fatal = false )
    {
        error++ ;
        auditor( oss, fatal ? Error : Warning );
        assert(fatal);
    }

    std::shared_ptr<iPLDevice<sysCDevice>> pl_dev; // raii on the TLM device
};

int main(int argc, char* argv[]){ MyApp( argc, argv ).run(); }
