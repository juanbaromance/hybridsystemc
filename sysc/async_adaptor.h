#ifndef ASYNC_ADAPTOR_H
#define ASYNC_ADAPTOR_H

#include "tlmx_channel.h"
#include "tlm_utils/simple_initiator_socket.h"
#include <systemc>
#include <thread>
#include <mutex>
#include "util.h"
#include "ipl.h"

struct async_adaptor_module : sc_core::sc_module
{
public:
    tlm_utils::simple_initiator_socket<async_adaptor_module> initiator_socket;

    async_adaptor_module(sc_core::sc_module_name instance_name, ipl_module::tlm_sock & target_sock );
    virtual ~async_adaptor_module(void);

    // SC_MODULE callbacks
    void before_end_of_elaboration(void) override;
    void end_of_elaboration(void) override;
    void start_of_simulation(void) override;
    void end_of_simulation(void) override;

private:
    static constexpr int debug = 2;

    // SystemC threads
    void initiator_thread(void);
    void keep_alive_thread(void);
    sc_core::sc_signal<bool> m_keep_alive_signal;

    // External OS thread
    void tcpip_thread();
    int  tcp_accept();
    int  tcp_sock;

    tlmx_channel initiator_channel;

    typedef void (*sig_t) (int);
    static void sighandler(int sig);

    int          m_tcpip_port;
    std::mutex   m_allow_pthread; //< must be declared before m_lock_permission
    std::unique_ptr<std::lock_guard<std::mutex>> m_lock_permission; //< must be declared before m_pthread
    std::thread  m_pthread;
    static int   s_stop_requests;

    static constexpr const char *whoami="async_adaptor_module";
    void auditor( const std::string & backtrace )
    {
        SC_REPORT_INFO( whoami, backtrace.c_str() );
    }
};

#endif
