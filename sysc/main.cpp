
// $Info: Prototypical sc_main $
//
// This is a slightly fancier sc_main than traditionally used that manages
// exceptions and errors more gracefully than the simplistic version used by
// most SystemC examples.

#include "top.h"
#include "report.h"
using namespace sc_core;
using namespace std;

static constexpr const char *whoami="main";

#include <memory>
int sc_main( int argc, char *argv[] )
{

  util::pause_on_exit if_windows;
  unique_ptr<top> top_instance;
  try
  {
      top_instance = make_unique<top>("top_instance");
  }
  catch ( std::exception& e )
  {
      SC_REPORT_ERROR( whoami,(string(e.what())+" Please fix elaboration errors and retry.").c_str());
      return 1;
  }
  catch (...) {
      SC_REPORT_ERROR(whoami,"Caught exception during elaboration");
      return 1;
  }

  try
  {
    SC_REPORT_INFO(whoami,"Starting sC-kernel");
    sc_start();
    SC_REPORT_INFO(whoami,"Exited sC-kernel");
  }
  catch ( std::exception& e ) {
      SC_REPORT_WARNING(whoami,(string("Caught exception ")+e.what()).c_str());
  } catch (...) {
      SC_REPORT_ERROR(whoami,"Caught exception during simulation.");
  }
  if (not sc_end_of_simulation_invoked())
  {
    SC_REPORT_INFO(whoami,"ERROR: Simulation stopped without explicit sc_stop()");
    sc_stop();
  }

  return util::errors() ? 1 : 0;
}
