// $Info: Simple device implementation $

#include "ipl.h"
#include "sc_literals.h"
using namespace sc_core;

SC_HAS_PROCESS(ipl_module);

ipl_module::ipl_module( sc_module_name instance_name )
    : sc_module(instance_name),
      target_socket("ipl-sock"), m_register_count(8) , m_latency(10_ns)
{
    m_byte_width = target_socket.get_bus_width()/8;
    target_socket.register_b_transport   ( this, & ipl_module::blocking_transport   );
    target_socket.register_transport_dbg ( this, & ipl_module::debug_transport      );

    std::stringstream oss;
    oss << "Constructed ipl.mem( " << m_register.size() << ")( " << m_latency << ")(BT/DebugT)" << name();
    auditor( oss.str( ) );
}

ipl_module::~ipl_module(void){ auditor( std::string("Destroyed ") + name() ); }


void ipl_module::blocking_transport( tlm::tlm_generic_payload & tgp, sc_time & delay )
{
    tlm::tlm_command command = tgp.get_command();
    sc_dt::uint64    address = tgp.get_address();
    unsigned char*   data_ptr = tgp.get_data_ptr();
    unsigned int     data_length = tgp.get_data_length();
    unsigned char*   byte_enables = tgp.get_byte_enable_ptr();
    unsigned int     streaming_width = tgp.get_streaming_width();

    // Rules
    if ( address >= no_registers * m_byte_width ) {
        tgp.set_response_status( tlm::TLM_ADDRESS_ERROR_RESPONSE );
        return;
    }

    if ( address % m_byte_width ) {
        // Only allow aligned bit width transfers
        SC_REPORT_WARNING( whoami, "Misaligned address");
        tgp.set_response_status( tlm::TLM_ADDRESS_ERROR_RESPONSE );
        return;
    }

    if ( byte_enables != nullptr) {
        // No support for byte enables
        tgp.set_response_status( tlm::TLM_BYTE_ENABLE_ERROR_RESPONSE );
        return;
    }

    if ((data_length % m_byte_width) != 0 || streaming_width < data_length || data_length == 0
        || (address+data_length)/m_byte_width >= no_registers ) {
        // Only allow word-multiple transfers within memory size
        tgp.set_response_status( tlm::TLM_BURST_ERROR_RESPONSE );
        return;
    }


    if ( command == tlm::TLM_READ_COMMAND )
    {
        memcpy( data_ptr, & m_register[ address/m_byte_width ], data_length);
    } else if ( command == tlm::TLM_WRITE_COMMAND )
    {
        memcpy( & m_register[ address/m_byte_width ], data_ptr, data_length);
    }
    else
    {
        tgp.set_response_status( tlm::TLM_COMMAND_ERROR_RESPONSE );
        return;
    }

    // Memory access time per bus value
    delay += ( m_latency * data_length/m_byte_width);
    tgp.set_response_status( tlm::TLM_OK_RESPONSE );
}


unsigned int ipl_module::debug_transport(tlm::tlm_generic_payload & tgp )
{
    tlm::tlm_command command = tgp.get_command();
    sc_dt::uint64    address = tgp.get_address();
    unsigned char*   data_ptr = tgp.get_data_ptr();
    unsigned int     data_length = tgp.get_data_length();
    unsigned char*   byte_enables = tgp.get_byte_enable_ptr();
    unsigned int     streaming_width = tgp.get_streaming_width();

    // Obliged to check address range and check for unsupported features,
    //   i.e. byte enables, streaming, and bursts
    // Can ignore DMI hint and extensions
    // Using the SystemC report handler is an acceptable way of signalling an error
    if ( address >= no_registers * m_byte_width )
    {
        tgp.set_response_status( tlm::TLM_ADDRESS_ERROR_RESPONSE );
        return 0;
    }
    if (address % m_byte_width)
    {
        // Only allow aligned bit width transfers
        tgp.set_response_status( tlm::TLM_ADDRESS_ERROR_RESPONSE );
        return 0;
    }

    // Obliged to implement read and write commands
    if ( command == tlm::TLM_READ_COMMAND ) {
        memcpy(data_ptr, &m_register[address/m_byte_width], data_length);
    } else if ( command == tlm::TLM_WRITE_COMMAND ) {
        memcpy(&m_register[address/m_byte_width], data_ptr, data_length);
    }
    else{
        tgp.set_response_status( tlm::TLM_COMMAND_ERROR_RESPONSE );
        return 0;
    }

    tgp.set_response_status( tlm::TLM_OK_RESPONSE );
    return data_length;
}

