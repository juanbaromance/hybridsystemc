
#include "top.h"
#include "async_adaptor.h"
#include "report.h"

using namespace sc_core;

top::top( sc_module_name instance_name ) : sc_module(instance_name)
      ,setup(whoami)
      ,ipl( std::make_unique<ipl_module>("ipl_instance") )
      ,async_adapter( std::make_unique<async_adaptor_module>( "async_adapter_instance", ipl->sock() ))

{
    SC_HAS_PROCESS(top);
    SC_THREAD(top_thread)
    auditor(": built");
}


