//BEGIN netlist.cpp (systemc)
///////////////////////////////////////////////////////////////////////////////
// $Info: Netlisting utility $

#include "netlist.h"
#include "report.h"
#include <typeinfo>
#include <iostream>
#include <iomanip>
#include <sstream>
#include <vector>
#include <string>
using namespace sc_core;
using namespace std;

namespace
{

// Traverse the entire object subhierarchy 
// below a given object

#include <type_traits>
template <typename T=false_type>
static void scan_hierarchy( sc_object* obj,string indent ) {

    if constexpr ( std::is_same<T,bool>::value )
    {
        std::vector<sc_object*> child = obj->get_child_objects();
        for ( unsigned int i=0; i != child.size(); i++ ) {
            sc_port_base*   iport = dynamic_cast<sc_port_base*>(child[i]);
            if (iport != 0) {
//                for (size_t p=0; p!=iport->size(); ++p) {
//                    sc_channel*      chan = dynamic_cast<sc_channel*>(iport->get_interface());
//                    if (chan != 0) {
//                        REPORT_INFO( indent << ' ' << child[i]->basename() << ":" << child[i]->kind() << " -> " << chan->name());
//                    }//endif
//                    sc_prim_channel* prim = dynamic_cast<sc_prim_channel*>(iport->get_interface());
//                    if (prim != 0) {
//                        REPORT_INFO( indent << ' ' << child[i]->basename() << ":" << child[i]->kind() << " -> " << prim->name());
//                    }//endif
//                    sc_port_base* tiport = dynamic_cast<sc_port_base*>(iport->get_interface());
//                    if (tiport != 0) {
//                        REPORT_INFO( indent << ' ' << child[i]->basename() << ":" << child[i]->kind() << " -> " << tiport->name());
//                    }//endif
//                    sc_export_base* txport = dynamic_cast<sc_export_base*>(iport->get_interface());
//                    if (txport != 0) {
//                        REPORT_INFO( indent << ' ' << child[i]->basename() << ":" << child[i]->kind() << " -> " << txport->name());
//                    }//endif
//                }//endfor
            }
            else {
                std::cout << indent << ' ' << child[i]->basename() << ":" << child[i]->kind();
                if ( child[i] ) {
                    scan_hierarchy<T>(child[i],string("| ")+indent);
                }

            }
        }
    }
}

}


namespace util {

void netlist(void) {

    std::vector<sc_object*> tops = sc_get_top_level_objects();
    std::cout << __PRETTY_FUNCTION__ ;
    for ( unsigned int i=0; i != tops.size(); i++ ) {
        if ( tops[i] ) {
            std::cout << tops[i]->name() << ":" << tops[i]->kind();
            scan_hierarchy<>(tops[i],"+-");
        }
    }
}

}
