// $Info: async_adaptor interface implementation $
//
// BRIEF DESCRIPTION:
// Implements a TLM initiator that performs transactions via TCP IP socket
// information.
//
// DETAILED DESCRIPTION:
//
// 1. Async_os_thread sets up TCP/IP socket to receive TLMX packets.
// 2. When a packet is received a push is executed on the thread-safe
//    async_channel, which generates a SystemC event.
// 3. initiator_thread wakes up, gets the data from the async_channel, formats
//    it for TLM 2.0 and initiates a transport call out its TLM 2.0 initiator
//    socket.
// 4. Target processes and returns a response
// 5. initiator_thread formats for TLMX and puts the response into the
//    async_channel, which generates an OS event
// 6. async_os_thread wakes up and pulls the data from async_channel and sends
//    back to the external connection via the TCP/IP socket connection.
//
// +----------+  recv  +------+ push  +-------+ event +---------+           +------+
// |External  |==TLMX=>|async |=tlmx=>|async  |------>|initiator|           |TLM2.0|
// |OS thread |        |_os_  |       |channel|    get|_sysc_   | transport |target|
// |or process|        |thread|       |       |=tlmx=>|thread_  |=tlm2=====>|      |
// |          |        |      |       |       |       |process  |           |      |
// |          |        |      | event |       |    put|         |<==========|      |
// |          |        |      |<------|       |<=tlmx=|         |           |      |
// |          |        |      |       |       |       |         |           |      |
// |          |   send |      | pull  |       |       |         |           |      |
// |          |<=TLMX==|      |<======|       |       |         |           |      |
// +----------+        +------+       +-------+       +---------+           +------+

#include "async_adaptor.h"
#include "report.h"
#include <iomanip>
#include <sys/socket.h>
#include <sys/errno.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <signal.h>
#include <unistd.h>

using namespace std;
using namespace sc_core;
using namespace sc_dt;

namespace {
  static char const* const MSGID = "async_adaptor";
}

int async_adaptor_module::s_stop_requests{0};

async_adaptor_module::async_adaptor_module(sc_module_name instance_name, ipl_module::tlm_sock & target_sock )
    : sc_module(instance_name)
    , initiator_socket("initiator_socket")
    , initiator_channel("m_async_channel")
    , m_keep_alive_signal("m_keep_alive_signal")

    , m_tcpip_port(4000)
    , m_lock_permission(new std::lock_guard<std::mutex>(m_allow_pthread))
    , m_pthread( & async_adaptor_module::tcpip_thread, this )
{
    signal(SIGINT,&async_adaptor_module::sighandler); //< allow for graceful interrupts
    for (int i=1; i<sc_argc(); ++i)
    {
        string arg(sc_argv()[i]);
        if      (arg.find("-debug")  == 0) sc_report_handler::set_verbosity_level(SC_DEBUG);
        else if (arg.find("-quiet")  == 0) sc_report_handler::set_verbosity_level(SC_NONE);
        else if (arg.find("-low")    == 0) sc_report_handler::set_verbosity_level(SC_LOW);
        else if (arg.find("-medium") == 0) sc_report_handler::set_verbosity_level(SC_MEDIUM);
        else if (arg.find("-high")   == 0) sc_report_handler::set_verbosity_level(SC_HIGH);
        else if (arg.find("-full")   == 0) sc_report_handler::set_verbosity_level(SC_FULL);
        else if (arg.find("-port=")  == 0) {
            m_tcpip_port = atoi(arg.substr(6).c_str());
        }
    }
    initiator_socket( target_sock );

    REPORT_INFO("\n===================================================================================\n"
                << "CONFIGURATION\n"
                << ">   Listening on port " << m_tcpip_port << "\n"
                << ">   Verbosity is " << sc_report_handler::get_verbosity_level() << "\n"
                << "===================================================================================\n"
                );

    SC_HAS_PROCESS(async_adaptor_module);
    SC_THREAD(initiator_thread)
    SC_THREAD(keep_alive_thread)
    REPORT_INFO("Constructed " << name());
}

async_adaptor_module::~async_adaptor_module          (void){ REPORT_INFO( "Destroyed " << name()); }
void async_adaptor_module::before_end_of_elaboration (void){ REPORT_INFO( __func__ << " " << name()); }
void async_adaptor_module::end_of_elaboration        (void){ REPORT_INFO( __func__ << " " << name()); }
void async_adaptor_module::end_of_simulation         (void){ REPORT_INFO( __func__ << " " << name()); }
void async_adaptor_module::start_of_simulation(void)
{
    std::cout << std::endl;
    REPORT_INFO( __func__ << " " << name() );
    m_keep_alive_signal.write( true );
    m_lock_permission.reset( nullptr );
}

int async_adaptor_module::tcp_accept()
{
    struct sockaddr_in local_server;
    int option_value;

    int listening_socket = socket(AF_INET, SOCK_STREAM, 0);
    if (listening_socket == -1) {
        REPORT_FATAL("Could not create socket");
    }

    // Prepare the sockaddr_in structure
    local_server.sin_family = AF_INET;
    local_server.sin_addr.s_addr = INADDR_ANY;
    local_server.sin_port = htons( m_tcpip_port );

    option_value = 1;
    if ( setsockopt( listening_socket, SOL_SOCKET, SO_REUSEADDR, (void* )&option_value, (socklen_t)(sizeof(socklen_t))) < 0 ) {
        REPORT_FATAL("Unable to set socket option");
    }

    if ( bind( listening_socket, (struct sockaddr *)&local_server , sizeof(local_server)) < 0 ) {
        REPORT_FATAL("Bind failed");
        exit(1);
    }

    REPORT_INFO("Queueing incoming connections...");
    listen( listening_socket, 1 );
    {
        std::lock_guard<std::mutex> request_permission(m_allow_pthread);
    }

    REPORT_INFO("Waiting for incoming connections...");
    int addr_len = sizeof(struct sockaddr_in);
    struct sockaddr_in remote_client;
    int incoming_socket = accept( listening_socket, (struct sockaddr *)&remote_client, (socklen_t*)&addr_len );
    if (incoming_socket<0) {
        REPORT_FATAL("Accept failed: " << strerror(errno));
    }
    else
        REPORT_INFO("Connection accepted...");
    return incoming_socket;

}

void async_adaptor_module::tcpip_thread()
{
    constexpr const char* backtrace = __PRETTY_FUNCTION__ ;
    std::cout << std::endl;
    REPORT_INFO("Starting " <<  backtrace );
    tcp_sock = tcp_accept();

    for( ;; )
    {
        std::array<char,TLMX_MAX_BUFFER> buffer;
        if( int recv_count = read( tcp_sock, buffer.data(), TLMX_MAX_BUFFER ); recv_count < 0 ) {
            REPORT_ERROR("TCPIP read/recv failed" << strerror(errno) ) continue;
        }
        else if (recv_count < TLMX_DATA_PTR_INDEX){ REPORT_FATAL("Incomplete packet received"); continue;  }
        else if constexpr ( debug > 3 )
            for ( size_t i = 0; i < recv_count; i++ )
                printf("%02x\n", ( static_cast<uint8_t>( buffer[i]) ) );

        std::array<uint8_t,TLMX_MAX_DATA_LEN> data_ptr;
        tlmx_packet_ptr packet( new tlmx_packet( TLMX_IGNORE, 0, 0, data_ptr.data() ));
        size_t unpacked_size = packet->unpack( buffer.data() );

        if constexpr ( debug > 1 ) std::cout << packet->print( __PRETTY_FUNCTION__ );

        {
            initiator_channel.push( packet );
            if constexpr ( debug > 2 )
                    REPORT_INFO("TLM.packet Pushed to initiator-thread : Waiting SystemC interrupt..");
            do {
                initiator_channel.wait_for_put();
                if ( not initiator_channel.nb_pull( packet ) )
                {
                    REPORT_WARNING("Missing response");
                    continue;
                }
                break;
            } while( 1 );
        }

        if (packet->status != TLMX_OK_RESPONSE)
        {
            REPORT_WARNING(tlmx_status_to_str(tlmx_status_t(packet->status)));
        }

        int packed_size = packet->pack(buffer.data());
        sc_assert(packed_size == unpacked_size);
        if constexpr ( debug > 1 )
                REPORT_INFO(packet->print("ack"));

        if( int send_count = write(tcp_sock, buffer.data(), packed_size); send_count < 0)
        {
            REPORT_ERROR("TCPIP write/send failed" << strerror(errno))
        }
        else
            sc_assert( send_count == packed_size);

        if ( packet->command == TLMX_EXIT )
        {
            REPORT_NOTE("Exiting due to TLMX_EXIT...");
            break;
        }

    }

    REPORT_INFO("Closing down...");
    close( tcp_sock );

}


void async_adaptor_module::initiator_thread(void)
{
    static constexpr const char* backtrace  = __PRETTY_FUNCTION__ ;
    std::cout << std::endl;
    REPORT_INFO( "Started " << __func__ << " " << name());

    for(;;)
    {
        array<uint8_t,TLMX_MAX_DATA_LEN> data_ptr;
        m_keep_alive_signal.write(true);
        wait( initiator_channel.sysc_put_event());
        m_keep_alive_signal.write(false);

        tlmx_packet_ptr tlm_pack( std::make_shared<tlmx_packet>( TLMX_IGNORE, 0, 0, data_ptr.data() ));
        if ( not initiator_channel.nb_get( tlm_pack ) )
            REPORT_WARNING("Missing response");

        if constexpr ( debug > 2 ) std::cout << tlm_pack->print( backtrace );
        tlm::tlm_generic_payload tgp;
        tgp.set_address         ( tlm_pack->address         );
        tgp.set_data_ptr        ( static_cast<unsigned char*>(tlm_pack->data_ptr) );
        tgp.set_data_length     ( tlm_pack->data_len        );
        tgp.set_streaming_width ( tlm_pack->data_len        );
        tgp.set_byte_enable_ptr ( nullptr                      );
        tgp.set_dmi_allowed     ( false                        );
        tgp.set_response_status ( tlm::TLM_INCOMPLETE_RESPONSE );


        switch( tlm_pack->command ) {
        case      TLMX_IGNORE: tgp.set_command(tlm::TLM_IGNORE_COMMAND); break;
        case       TLMX_WRITE: tgp.set_command(tlm::TLM_WRITE_COMMAND); break;
        case TLMX_DEBUG_WRITE: tgp.set_command(tlm::TLM_WRITE_COMMAND); break;
        case        TLMX_READ: tgp.set_command(tlm::TLM_READ_COMMAND); break;
        case  TLMX_DEBUG_READ: tgp.set_command(tlm::TLM_READ_COMMAND); break;
        default :
            REPORT_WARNING("Unknown TLMX command - ignored");
            break;
        }

        // Strategy pattern
        switch ( tlm_pack->command )
        {
        case TLMX_DEBUG_READ:
        case TLMX_DEBUG_WRITE:
            if ( initiator_socket->transport_dbg( tgp ) != tlm_pack->data_len )
                REPORT_WARNING("transport_dbg returned failure" << tlm_pack->data_len );
            break;

        default :
        {
            sc_time delay{SC_ZERO_TIME};
            initiator_socket->b_transport( tgp, delay );
            wait( delay );
            break;
        }
        }

        // Update tlmx_packet
        switch ( tgp.get_response_status() ) {
        case tlm::TLM_OK_RESPONSE:
            tlm_pack->status = TLMX_OK_RESPONSE;
            break;
        case tlm::TLM_ADDRESS_ERROR_RESPONSE: tlm_pack->status = TLMX_ADDRESS_ERROR_RESPONSE; break;
        case tlm::TLM_INCOMPLETE_RESPONSE: tlm_pack->status = TLMX_INCOMPLETE_RESPONSE;    break;
        default :
            tlm_pack->status = TLMX_GENERIC_ERROR_RESPONSE; break;
        }

        initiator_channel.nb_put( tlm_pack );
        if constexpr ( debug > 2 )
            std::cout << tlm_pack->print( backtrace );

        if( tlm_pack->command  == TLMX_EXIT )
            break;
    }

    wait(1,SC_SEC);
    REPORT_INFO("Exiting async_adaptor interface");
    sc_stop();
}


// In the event there is nothing else happening, this process will keep the simulator from starving.
void async_adaptor_module::keep_alive_thread(void)
{

    REPORT_INFO("Started " << __func__ << " " << name());
    for(;;) {
        if ( s_stop_requests > 0 )
            break;

        if (!m_keep_alive_signal.read())
        {
            wait(m_keep_alive_signal.default_event());
        }
        wait(SC_ZERO_TIME);
    }
    wait(1,SC_SEC);
    REPORT_INFO("Exiting due to stop request.");
    sc_stop();
}

void async_adaptor_module::sighandler( int sig ){ ++s_stop_requests; }
