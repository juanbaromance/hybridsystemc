#pragma once

// Top-level module where everything is hooked up.
// Basic top-level module where everything is hooked up.
//
// +------------------------------+
// |  top_instance                |
// |                              |
// |  +------------------------+  |
// |  | async_adaptor_instance |  |
// |  +-----------V------------+  |
// |              V  SIS(Simple Initiator Socket)
// |              |               |
// |              V  STS(Simple Target Socket)
// |  +-----------V------------+  |
// |  | ipl_instance           |  |
// |  +------------------------+  |
// |                              |
// +------------------------------+

#include <systemc>
#include <memory>
#include "report.h"
#include "async_adaptor.h"
#include "ipl.h"
#include "netlist.h"


class top: public sc_core::sc_module
{
private:
    util::report setup;

    std::unique_ptr<ipl_module>  ipl;
    std::unique_ptr<async_adaptor_module> async_adapter;

    static constexpr const char *whoami="top_module";
    void auditor( const std::string & backtrace ){ util::sc_auditor::report( whoami, backtrace); }

public:
    top( sc_core::sc_module_name instance_name );

    virtual ~top(void) { auditor( "Destroyed" ); }
    void before_end_of_elaboration( void ) override { auditor( __PRETTY_FUNCTION__ ); }
    void end_of_elaboration  ( void ) override { auditor( __PRETTY_FUNCTION__ ); util::netlist(); } //< e.g. add sc_trace
    void start_of_simulation ( void ) override { auditor( __PRETTY_FUNCTION__ ); } //< e.g. channel initializations
    void end_of_simulation   ( void ) override { auditor( __PRETTY_FUNCTION__ ); } //< e.g. cleanup, statistics

    // Processes
    void top_thread( void ) { auditor( __PRETTY_FUNCTION__ ); }
};
