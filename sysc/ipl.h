#pragma once

#include <systemc>
#include "tlm_utils/simple_target_socket.h"
#include <array>
#include <string>
#include <report.h>

struct ipl_module : sc_core::sc_module
{
public:
    ipl_module( sc_core::sc_module_name instance_name  );
    virtual ~ipl_module(void);
    using tlm_sock = tlm_utils::simple_target_socket<ipl_module>;
    tlm_sock & sock() { return target_socket; };

    /* TLM socket callbacks */
private:
    void blocking_transport(tlm::tlm_generic_payload & tgp, sc_core::sc_time& delay );
    unsigned int  debug_transport( tlm::tlm_generic_payload & tgp );

    static constexpr int debug = 2;
    static constexpr const char *whoami="ipl_module";
    void auditor( const std::string & backtrace ){ util::sc_auditor::report( whoami, backtrace); }
    constexpr static sc_dt::uint64 no_registers = 8;
    sc_dt::uint64    m_register_count;
    std::array<int32_t,no_registers> m_register;
    tlm_sock target_socket;
    int              m_byte_width;     //< byte width of socket
    sc_core::sc_time m_latency;

};
